/* Requiring necessary packages */
var gulp = require('gulp'),
	autoprefixer = require('gulp-autoprefixer'),
	browsersync = require('browser-sync'),
	changed = require('gulp-changed'),
	concat = require('gulp-concat'),
	cssnano = require('gulp-cssnano'),
	uglify = require('gulp-uglify'),
	htmlmin = require('gulp-htmlmin'),
	tinypng = require('gulp-tinypng-compress'),
	sass = require('gulp-sass'),
	sourcemaps = require('gulp-sourcemaps');

/* Setting base project constants */
const paths = {
	src: './src/',
	dest: '../wp-content/themes/erb-facis2018/'
};

/* Setting an error swallower */
var swallowError = function(error) {
	console.log(error.toString())
	this.emit('end')
}

/*
* BASIC
*
* Those listed below are the basic tasks
* for compiling & distributing files
*/
gulp.task('php', function() {
	gulp.src([paths.src + '**/*.php'])
	.pipe(changed(paths.dest))
	.pipe(gulp.dest(paths.dest));
});

gulp.task('css', function() {
	gulp.src([paths.src + 'scss/**/*.scss'])
	.pipe(changed(paths.dest))
	.pipe(sass({includePaths:['scss/**']}))
	.on('error', swallowError)
	.pipe(autoprefixer())
	.pipe(cssnano({zindex: false}))
	.pipe(concat('style.css'))
	.pipe(gulp.dest(paths.dest));
});

gulp.task('js', function() {
	gulp.src([paths.src + 'js/**/*.js'])
	.pipe(changed(paths.dest + 'js'))
	.pipe(uglify())
	.pipe(concat('scripts.min.js'))
	.pipe(gulp.dest(paths.dest + 'js'));
});

gulp.task('img', function() {
	// Setting allowed images
	gulp.src([
		paths.src + 'img/*.jpg',
		paths.src + 'img/*.gif',
		paths.src + 'img/*.png',
		paths.src + 'img/*.svg',
		paths.src + 'img/*.mp4'
		])
	.pipe(changed(paths.dest + 'img'))
	.pipe(tinypng({
			key: 'znQaEIjGEHt8nKJc5cXHAkOOxMbCnWf4',
			sigFile: 'images/.tinypng-sigs',
			log: false
		}))
	.pipe(gulp.dest(paths.dest + 'img'));

	gulp.src([
		paths.src + '*.jpg',
		paths.src + '*.png',
		])
	.pipe(tinypng({
			key: 'znQaEIjGEHt8nKJc5cXHAkOOxMbCnWf4',
			sigFile: 'images/.tinypng-sigs',
			log: false
		}))
	.pipe(gulp.dest(paths.dest));
});

gulp.task('fonts', function() {
	gulp.src(['node_modules/font-awesome/fonts/*.*'])
	.pipe(gulp.dest(paths.dest + 'fonts'));
});

gulp.task('libs', function() {
	/* 
	* Here comes all the third-party files
	* like Fontawesome, bulma...
	*/

	// CSS Libs
	gulp.src([
		// 'node_modules/bootstrap/dist/css/bootstrap.min.css',
		'node_modules/slick-carousel/slick/slick.scss',	
		'node_modules/normalize.css/normalize.scss',
		'node_modules/font-awesome/scss/font-awesome.scss',
		'node_modules/izimodal/css/iziModal.min.css',
		])
	.pipe(changed(paths.dest + 'css'))
	.pipe(sass())
	.pipe(autoprefixer())
	.pipe(cssnano())
	.pipe(concat('libs.min.css'))
	.pipe(gulp.dest(paths.dest + 'css'));

	// JS Libs
	gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/slick-carousel/slick/slick.min.js',
		'node_modules/izimodal/js/iziModal.min.js',
		'node_modules/vanilla-masker/build/vanilla-masker.min.js',
		])
	.pipe(changed(paths.dest + 'js/libs'))
	.pipe(concat('libs.min.js'))
	.pipe(gulp.dest(paths.dest + 'js/libs'));
});

gulp.task('watch', function() {
	var php = gulp.watch([paths.src + '**/*.php'], ['php']),
	css = gulp.watch([paths.src + 'scss/**/*.scss'], ['css']),
	js = gulp.watch([paths.src + 'js/**/*.js'], ['js']);

	browsersync.init([paths.dest], {
		//proxy: 'http://localhost:8888/', //for mac!
		proxy: 'http://localhost/',
		browser: 'google chrome',
		port: 3042,
		notify: false
	});
});

/*
* SERVER
* This task compiles every file in
* the project, without starting
* browsersync for development
*/
gulp.task('server', ['php', 'css', 'js', 'img', 'fonts', 'libs']);

/*
* GLOBAL
*
* This task runs everything in basic
* task list, except "Deploy task"
*/
gulp.task('default', ['php', 'css', 'js', 'img', 'libs', 'watch']);