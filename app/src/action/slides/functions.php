<?php

////////////////////////////////////////////////////////////////////////
// Slides
////////////////////////////////////////////////////////////////////////


add_action( 'init', 'slide_post_type' );
function slide_post_type() {
	register_post_type( 'slide',
		array(
			'labels' => array(
				'name' 				=> 'Slides',
				'singular_name' 	=> 'Slides',
				'menu_name'         => 'Slides',
				'all_items'         => 'Todos os Slides',
				'view_item'         => 'Ver Slide',
				'add_new_item'      => 'Adicionar novo Slide',
				'add_new'           => 'Adicionar Slide',
				'edit_item'         => 'Alterar Slide',
				'update_item'       => 'Atualizar Slide',
				'search_items'      => 'Pesquisar Slide',
				'not_found'         => 'Nenhum Slide Encontrado',
				'not_found_in_trash'=> 'Nenhum Slide Encontrado na Lixeira',
			),
		'hierarchical' 		  => true,
		'has_archive' 		  => true,
		'public' 			  => true,
		'exclude_from_search' => false,
		'capability_type'     => 'post',
		'menu_icon' 		  => 'dashicons-images-alt',
    	'menu_position' => 5,
		'supports'            => array( 'title', 'thumbnail', 'page-attributes'),
		'rewrite'             => array( 'slug' => 'slides'),
    	)
	);
	
	flush_rewrite_rules();
}