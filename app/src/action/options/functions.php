<?php

/*
*  Registrando paginas em opções do tema'
*/

if (function_exists('acf_set_options_page_title')) {
	acf_set_options_page_title(__('Opções do tema'));
}

if (function_exists('acf_add_options_sub_page')) {
	acf_add_options_sub_page('Home');
	acf_add_options_sub_page('Infos');
}

?>