<?php

/* Arquivos Externos */
include_once('action/options/functions.php');
include_once('action/slides/functions.php');

/* Menus */

register_nav_menus( array(
		'principal' => __( 'Menu Principal', '' ),
		'topo' => __( 'Menu Topo', '' ),
	) );

function fallbackmenu(){ ?>
							<ul><li> Go to Adminpanel > Appearance > Menus to create your menu. You should have WP 3.0+ version for custom menus to work.</li></ul>
<?php }	

/* FEATURED THUMBNAILS */

if ( function_exists( 'add_theme_support' ) ) { // Added in 2.9
	add_theme_support( 'post-thumbnails');
    add_image_size('thumb-cursos', 360, 300, true );
}

/* GET THUMBNAIL URL */

function get_image_url(){
	$image_id = get_post_thumbnail_id();
	$image_url = wp_get_attachment_image_src($image_id,'large');
	$image_url = $image_url[0];
	echo $image_url;
    }


function cadastro_ajax(){

$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$celular = $_POST['celular'];
$mensagem = $_POST['mensagem'];

/* Sends the form */
$to = 'contato@alianzamagistral.com.br';
$subject = 'Formulário de Contato';
$message = '<p><strong>Dados do contato</strong></p>
            <p>Nome: <strong>'.$nome.'</strong></p>
            <p>E-mail: <strong>'.$email.'</strong></p>
            <p>Telefone: <strong>'.$telefone.'</strong></p>
            <p>Celular: <strong>'.$celular.'</strong></p>
            <p>Mensagem: <strong>'.$mensagem.'</strong></p>
           ';
$headers = 'Content-Type: text/html; charset=UTF-8'.
					 'From: contato@alianzamagistral.com.br' . "\r\n" .
           'Reply-To: '. $email . "\r\n" .
           'X-Mailer: PHP/' . phpversion();

$send = wp_mail( $to, $subject, $message, $headers );

die();

}

add_action( 'wp_ajax_cadastro_ajax', 'cadastro_ajax' );

add_action("wp_ajax_nopriv_cadastro_ajax", "cadastro_ajax");

function cadastro_orcamento(){

$nome = $_POST['nome'];
$email = $_POST['email'];
$farmacia = $_POST['farmacia'];
$cnpj = $_POST['cnpj'];
$telefone = $_POST['telefone'];
$celular = $_POST['celular'];
$cidade = $_POST['cidade'];
$estado = $_POST['estado'];
$mensagem = $_POST['mensagem'];

/* Sends the form */
$to = 'contato@alianzamagistral.com.br';
$subject = 'Formulário de Orçamento';
$message = '<p><strong>Dados do contato</strong></p>
            <p>Nome: <strong>'.$nome.'</strong></p>
            <p>E-mail: <strong>'.$email.'</strong></p>
            <p>Nome da Farmácia: <strong>'.$farmacia.'</strong></p>
            <p>CNPJ: <strong>'.$cnpj.'</strong></p>
            <p>Telefone: <strong>'.$telefone.'</strong></p>
            <p>Celular: <strong>'.$celular.'</strong></p>
            <p>Cidade: <strong>'.$cidade.'</strong></p>
            <p>Estado: <strong>'.$estado.'</strong></p>
            <p>Mensagem: <strong>'.$mensagem.'</strong></p>
           ';
$headers = 'Content-Type: text/html; charset=UTF-8'.
					 'From: contato@alianzamagistral.com.br' . "\r\n" .
           'Reply-To: '. $email . "\r\n" .
           'X-Mailer: PHP/' . phpversion();

$send = wp_mail( $to, $subject, $message, $headers );

die();

}

add_action( 'wp_ajax_cadastro_orcamento', 'cadastro_orcamento' );

add_action("wp_ajax_nopriv_cadastro_orcamento", "cadastro_orcamento");

function cadastro_modal(){

$nome = $_POST['nome'];
$email = $_POST['email'];
$telefone = $_POST['telefone'];
$celular = $_POST['celular'];
$ativo = $_POST['ativo'];

/* Sends the form */
$to = 'contato@alianzamagistral.com.br';
$subject = 'Formulário de Modal Download Apresentação';
$message = '<p><strong>Dados do contato</strong></p>
            <p>Nome: <strong>'.$nome.'</strong></p>
            <p>E-mail: <strong>'.$email.'</strong></p>
            <p>Telefone: <strong>'.$telefone.'</strong></p>
            <p>Celular: <strong>'.$celular.'</strong></p>
            <p>Ativo: <strong>'.$ativo.'</strong></p>
           ';
$headers = 'Content-Type: text/html; charset=UTF-8'.
           'From: contato@alianzamagistral.com.br' . "\r\n" .
           'Reply-To: '. $email . "\r\n" .
           'X-Mailer: PHP/' . phpversion();

$send = wp_mail( $to, $subject, $message, $headers );

die();

}

add_action( 'wp_ajax_cadastro_modal', 'cadastro_modal' );

add_action("wp_ajax_nopriv_cadastro_modal", "cadastro_modal");
?>