<?php 
/*Template name: Home */
get_header(); ?>

<!-- banner -->
<section class="banner">
	<div class="container full">
		<?php $slide = get_field('slide', 'options');

		foreach ($slide as $item) :
			?>
			<a href="<?php echo $item['link_do_slide'];?>">
				<img src="<?php echo $item['imagem'];?>" alt="<?php echo $item['titulo'];?>">
			</a>
		<?php endforeach; ?>
	</div>
	<div class="prev">
		<i class="fa fa-angle-left" aria-hidden="true"></i>		
	</div>
	<div class="next">
		<i class="fa fa-angle-right" aria-hidden="true"></i>		
	</div>
</section> 

<!-- seção de busca -->
<section class="search-bar">
	<div class="container flex center">
		<form method="get" id="searchform" action="<?php echo home_url( '/' ); ?>">
			<label for="search">
				<i class="fa fa-search" aria-hidden="true"></i>
			</label>
			<input type="search" id="search" name="s" class="input search" placeholder="O que você está buscando?">
		</form>
	</div>
</section>

<!-- seção ativos em destaque -->
<section class="featured">
	<div class="container flex">
		<hgroup class="block-header">
			<h3>Ativos</h3>
			<h2>Em destaque</h2>
		</hgroup>
		<ul class="featured-list">
			<?php $destaques = get_field('ativos_destaque', 'options');

			foreach ($destaques as $destaque) :
				?>

				<li class="featured-item">
					<a href="<?php echo $destaque['link']; ?>">
						<img src="<?php echo $destaque['imagem']; ?>" alt="<?php echo $destaque['nome']; ?>">
						<span>saiba mais</span>
					</a>
					<h4><?php echo $destaque['nome']; ?></h4>

					<p>
						<?php echo $destaque['frase']; ?>
					</p>
				</li>
			<?php endforeach; ?>
		</ul>
		<div class="prev-featured">
			<i class="fa fa-angle-left" aria-hidden="true"></i>		
		</div>
		<div class="next-featured">
			<i class="fa fa-angle-right" aria-hidden="true"></i>		
		</div>
	</div>
</section>

<!-- seção faça um orçamento -->
<section class="budget">
	<div class="container flex">
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/sample-bees.jpg" alt="">
			
		</figure>
		<div class="budget-form">			
			<hgroup class="block-header">
				<h3>Faça um</h3>
				<h2>Orçamento</h2>
			</hgroup>
			<form action="" method="POST" id="formOrcamento">
				<input class="input" type="text" name="nome" id="name" placeholder="Nome" required>
				<input class="input" type="email" name="email" id="email" placeholder="Email" required>
				<input class="input" type="text" name="farmacia" id="farmacia" placeholder="Nome da Farmácia" required>
				<input class="input" type="text" name="cnpj" id="cnpj" placeholder="CNPJ" required>
				<input class="input" type="phone" name="telefone" id="phone" placeholder="Telefone" required>
				<input class="input" type="phone" name="celular" id="mobile" placeholder="Celular" required>
				<input class="input" type="text" name="cidade" id="city" placeholder="Cidade" required>
				<input class="input" type="text" name="estado" id="state" placeholder="Estado" required>
				<textarea class="textarea" name="mensagem" id="message" placeholder="Mensagem" required></textarea>
				<button class="btn solid" type="submit">
					Enviar
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
				</button>
			</form>
			<div class="alert success">
				<i class="fa fa-check" aria-hidden="true"></i>
				<p>Seu e-mail foi cadastrado</p>
				<p>com sucesso!</p>
			</div>

			<div class="alert error">
				<i class="fa fa-times" aria-hidden="true"></i>
				<p>Por favor, preencha</p>
				<p>todos os campos</p>
			</div>
			
		</div>
	</div>
</section>
<!-- seçao notícias -->
<section class="news">
	<div class="container flex">
		<hgroup class="block-header">
			<h3>confira</h3>
			<h2>últimas notícias</h2>
		</hgroup>
		<ul class="news-list">
			<?php 
			$query = new WP_Query( array( 'post_type' => 'post', 'orderby'=> 'date', 'order' => 'ASC', 'posts_per_page'    => 3));
			if ( $query->have_posts() ) : 
				while ($query->have_posts()) : $query->the_post();
					$titulo = get_the_title();
					$link = get_the_permalink();
					$categories = get_the_category();
					$html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

					?>
					<li class="news-item">
						<figure>
							<img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>">
						</figure>
						<div class="news-content">					
							<strong>
								<?php echo $titulo; ?>
							</strong>
							<?php the_excerpt(); ?>
							<div class="news-links">
								<a href="<?php echo esc_url( get_category_link( $categories[0]->term_id ) ); ?>" class="category"><?php echo $categories[0]->name; ?></a>
								<a href="<?php echo $link; ?>" class="btn inverted">
									Saiba mais
									<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
								</a>

							</div>
						</div>
					</li>
					<?php 
				endwhile;
			endif;
			wp_reset_postdata(); ?>            
		</ul>

	</div>	

</section>
<?php get_footer(); ?>