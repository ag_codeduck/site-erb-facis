<?php get_header(); 

/* Template Name: Insumos */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

     $title = get_the_title();
     $missao = get_field('missao');
     $visao = get_field('visao');
     $valores = get_field('valores');
     $parceiros = get_field('parceiros');

    endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="featured insumos">
	<div class="container flex">
		<ul class="featured-list">
<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => $post->ID, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    // run the loop based on the query
    if ( $query->have_posts() ) { 
	while ($query->have_posts()) : $query->the_post();
		
	global $post;
	  $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
	  $titulo = get_the_title();
    $link = get_the_permalink();
    $destaque =  get_field('imagem_destaque');

	?>			
			<li class="featured-item">
				<a href="<?php echo $link; ?>">
					<img src="<?php echo $destaque; ?>" alt="<?php echo $titulo; ?>">
					<span>saiba mais</span>
				</a>
				<h4><?php echo $titulo; ?></h4>
			</li>
		<?php endwhile;
        //$myvariable = ob_get_clean();
        //return $myvariable;
        wp_reset_postdata(); }?>
		</ul>
		<div class="prev-featured">
			<i class="fa fa-angle-left" aria-hidden="true"></i>		
		</div>
		<div class="next-featured">
			<i class="fa fa-angle-right" aria-hidden="true"></i>		
		</div>
	</div>
</section>
<?php get_footer(); ?>