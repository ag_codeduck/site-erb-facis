<?php get_header(); 

/* Template Name: Ativos */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

     $title = get_the_title();
     $id = get_the_ID();
     $idPage = $id;
     $ativos = get_field('insumos');

    endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="product">
	<div class="container sidebar">
<?php if($idPage == 25){ ?>        
		<article class="content-list">
			<ul>
				<?php foreach ($ativos as $ativo) : ?>
                <li>
					<h3><?php echo $ativo['nome']; ?></h3>
					<em><?php echo $ativo['inci_name']; ?></em>
					<strong>funcionalidade</strong>
					<p>
						<?php echo $ativo['funcionalidade']; ?>
                    </p>
					<strong>Dow Corning</strong>
					<p>
						<?php echo $ativo['dow_corning']; ?>
                    </p>
					<strong>Momentive(GE)</strong>
					<p>
						<?php echo $ativo['momentive(ge)']; ?>
                    </p>
                    <a href="#" class="btn solid">
						Saiba mais
						<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					</a>
				</li>
                <?php endforeach; ?>
			</ul>
		</article>
<?php }else{ ?>
        <article class="content-list">
			<ul>
				<?php foreach ($ativos as $ativo) : ?>
                <li>
					<h3><?php echo $ativo['nome']; ?></h3>
					<em><?php echo $ativo['inci_name']; ?></em>
					<strong>funcionalidade</strong>
					<p>
						<?php echo $ativo['funcionalidade']; ?>
                    </p>
                    <a href="#" class="btn solid">
						Saiba mais
						<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					</a>
				</li>
                <?php endforeach; ?>
			</ul>
        </article>
<?php } ?>        
		<aside class="others-list">
			<h4>Confira todos os insumos da Alianza Magistral</h4>
			<ul>
<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
	  $titulo = get_the_title();
      $link = get_the_permalink();
      $idSide = get_the_ID();
    
    if($idPage == $idSide):
    ?>
				<li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
    <?php else: ?>
				<li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
    <?php 
    endif;
    endwhile;
    endif;
        wp_reset_postdata(); ?>            
			</ul>
		</aside>
	</div>
</section>


<?php get_footer(); ?>