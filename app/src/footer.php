<!-- seção newsletter -->

<section class="newsletter" style="display: none;">
	<div class="container flex">
		<form class="form" action="">
			<legend>Assine Nossa Newsletter</legend>
			<input type="text" class="input" placeholder="Nome">
			<input type="email" class="input" placeholder="Email">
			<button class="btn">
				Enviar
				<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
			</button>
		</form>
	</div>
</section>

<!-- seção de contato -->
<?php 
$email = get_field('e-mail','options');
$telefone = get_field('telefone','options');
$telefone2 = get_field('telefone2','options');
$whatsapp = get_field('whatsapp','options');
$link_facebook = get_field('link_facebook','options');
$link_instagram = get_field('link_instagram','options');
?>

<section class="contact-box" style="border-top:1px solid #4c0112">
	<div class="container flex">
		<div class="social-icons">
			<img src="<?php echo get_template_directory_uri(); ?>/img/alianza-magistral-big.png" alt="Alianza Magistral" title="Alianza Magistral">
			<ul class="social-bottom">
				<li><a href="<?php echo $link_facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
				<li><a href="<?php echo $link_instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
			</ul>
		</div>
		<div class="contact-info">
			<h5>Contato</h5>
			<ul>
				<li>
					<a href="mailto:<?php echo $email; ?>">
						<i class="fa fa-envelope" aria-hidden="true"></i>
						<?php echo $email; ?>
					</a>
				</li>
				<li>
					<a href="tel:+5511<?php echo $telefone; ?>">
						<i class="fa fa-phone" aria-hidden="true"></i>
						11 <?php echo $telefone; ?> / <?php echo $telefone2; ?>
					</a>
				</li>
				<li>
					<a href="tel:+5511<?php echo $whatsapp; ?>">
						<i class="fa fa-whatsapp" aria-hidden="true"></i>
						11 <?php echo $whatsapp; ?>
					</a>
				</li>
				<li>
					<i class="fa fa-map-marker" aria-hidden="true"></i>
					Rua Margarida Zingg, 245 <br>
					Jd Itacolomi - São Paulo - SP <br>
					CEP 04385-080
				</li>
			</ul>
		</div>
		<div class="fb-like-box">
			<div class="fb-page" data-href="https://www.facebook.com/AlianzaMagistral/" data-width="360" data-small-header="false" data-adapt-container-width="false" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/AlianzaMagistral/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/AlianzaMagistral/">Alianza Magistral</a></blockquote></div>
		</div>
	</div>
</section>
<footer class="footer">
	<div class="contact-footer">
		
	</div>
	<div class="bottom-footer">
		<div class="container flex between">			
			<span>
				2017 Alianza Magistral © - Todos os Direitos Reservados.			
			</span>
			<span>
				Desenvolvido por codeduck
			</span>
		</div>
	</div>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.26/webfont.js"></script>
<script>
	WebFont.load({
		google: {
			families: ['Lato:300,400,700','Open+Sans:300,400,700']
		}
	});
</script>
<script type="text/javascript">
	/* Layout CSS File */
	var css = document.createElement('link');
	css.rel = 'stylesheet';
	css.href = '<?php bloginfo('stylesheet_url'); ?>';
	css.type = 'text/css';
	var godefer = document.getElementsByTagName('link')[0];
	godefer.parentNode.insertBefore(css, godefer);

	/* Libs CSS File */
	var cssLibs = document.createElement('link');
	cssLibs.rel = 'stylesheet';
	cssLibs.href = '<?php echo get_template_directory_uri(); ?>/css/libs.min.css';
	cssLibs.type = 'text/css';
	var godefer = document.getElementsByTagName('link')[0];
	godefer.parentNode.insertBefore(cssLibs, godefer);
</script> 
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/libs/libs.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/scripts.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#formOrcamento').submit(ajaxSubmitOrcamento);

		function ajaxSubmitOrcamento(){

			var formOrcamento = $(this).serialize();

			$.ajax({
				type:"POST",
				url: "/wp-admin/admin-ajax.php?action=cadastro_orcamento",
				data: formOrcamento,
				success:function(data){
					$(".alert.success" ).css( "display", "flex");
					setTimeout(function() { $('.alert.success').fadeOut('slow');}, 3000);
					document.getElementById('name').value='';
					document.getElementById('email').value='';
					document.getElementById('farmacia').value='';
					document.getElementById('phone').value='';
					document.getElementById('mobile').value='';
					document.getElementById('city').value='';
					document.getElementById('state').value='';
					document.getElementById('cnpj').value='';
					document.getElementById('message').value='';
					console.log(formOrcamento);
				}
			});

			return false;
		}

		$('#newCustomerForm').submit(ajaxSubmit);

		function ajaxSubmit(){

			var newCustomerForm = $(this).serialize();

			$.ajax({
				type:"POST",
				url: "/wp-admin/admin-ajax.php?action=cadastro_ajax",
				data: newCustomerForm,
				success:function(data){
					$(".alert.success" ).css( "display", "flex");
					setTimeout(function() { $('.alert.success').fadeOut('slow');}, 3000);
					document.getElementById('name').value='';
					document.getElementById('email').value='';
					document.getElementById('phone').value='';
					document.getElementById('mobile').value='';
					document.getElementById('message').value='';
				}
			});

			return false;
		}
		$('#formModal').submit(ajaxSubmitModal);
		
		
		var  closeModal = $(".modal-close");

		function downloadPDF(){
			var pdf = document.getElementById('downloadPDF');
			pdf.click();
		}

		function ajaxSubmitModal(e){
			e.preventDefault()
			var formModal = $(this).serialize();
			
			$.ajax({
				type:"POST",
				url: "/wp-admin/admin-ajax.php?action=cadastro_modal",
				data: formModal,
				success:function(data){
					$(".alert.success" ).css( "display", "flex");
					setTimeout(function() { $('.alert.success').fadeOut('slow');}, 3000);
					document.getElementById('name').value='';
					document.getElementById('email').value='';
					document.getElementById('phone').value='';
					document.getElementById('mobile').value='';
					downloadPDF();
					closeModal.click();	
				}
			});

			return false;
		}
		
	});
</script>

<?php wp_footer(); ?>
</body>
</html>