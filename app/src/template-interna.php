<?php 
/*Template name: Interna */
get_header(); ?>

<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2>Titulo</h2>
		</hgroup>
	</div>
</section>


<section class="product">
	<div class="container sidebar">
		<article class="content-full">
			<picture>
				<img src="" alt="">
			</picture>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloribus ducimus ratione iure corporis, sint. Pariatur cum dignissimos, illum fugiat omnis natus dicta non, praesentium illo, obcaecati repellendus! Impedit, maxime, quod!</p>
		</article>
		<aside class="others-list">
			<h4>others list</h4>
			<ul>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>Lorem</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>ipsum</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>dolor</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>sit</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>amet</a></li>
			</ul>
		</aside>
	</div>
</section>

<section class="product">
	<div class="container sidebar">
		<article class="content-list">
			<ul>
				<li>
					<h3>nome</h3>
					<em>?????</em>
					<strong>funcionalidades</strong>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde eligendi laborum, quasi porro vitae facilis totam reiciendis hic blanditiis dicta aliquam, ea neque suscipit beatae eaque odio dignissimos, culpa similique!
					</p>
				</li>
				<li>
					<h3>nome</h3>
					<em>?????</em>
					<strong>funcionalidades</strong>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde eligendi laborum, quasi porro vitae facilis totam reiciendis hic blanditiis dicta aliquam, ea neque suscipit beatae eaque odio dignissimos, culpa similique!
					</p>
				</li>
				<li>
					<h3>nome</h3>
					<em>?????</em>
					<strong>funcionalidades</strong>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde eligendi laborum, quasi porro vitae facilis totam reiciendis hic blanditiis dicta aliquam, ea neque suscipit beatae eaque odio dignissimos, culpa similique!
					</p>
				</li>
				<li>
					<h3>nome</h3>
					<em>?????</em>
					<strong>funcionalidades</strong>
					<a href="#" class="trigger">Modal</a>
					<p>
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Unde eligendi laborum, quasi porro vitae facilis totam reiciendis hic blanditiis dicta aliquam, ea neque suscipit beatae eaque odio dignissimos, culpa similique!
					</p>
				</li>
			</ul>
		</article>
		<aside class="others-list">
			<h4>others list</h4>
			<ul>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>Lorem</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>ipsum</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>dolor</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>sit</a></li>
				<li><a href=""><i class="fa fa-caret-right" aria-hidden="true"></i>amet</a></li>
			</ul>
		</aside>
	</div>
</section>

<!-- modal padrão -->
<div id="modal" class="modal">
    <a href="#close-modal" title="Fechar" class="modal-close" data-izimodal-close="">x</a>
    
    <!-- conteudo do modal -->
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore facere qui et esse cumque illum veritatis, similique sunt assumenda nam accusamus aperiam repellendus libero alias sint explicabo necessitatibus mollitia maxime!</p>
    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore facere qui et esse cumque illum veritatis, similique sunt assumenda nam accusamus aperiam repellendus libero alias sint explicabo necessitatibus mollitia maxime!</p>

    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tempore facere qui et esse cumque illum veritatis, similique sunt assumenda nam accusamus aperiam repellendus libero alias sint explicabo necessitatibus mollitia maxime!</p>
</div>


<?php get_footer(); ?>