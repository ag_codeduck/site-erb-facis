<?php error_reporting(E_ERROR); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta name="theme-color" content="#000">
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
	<meta http-equiv="Content-Type" content="text/html;charset=windows-1251">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title><?php the_title(); ?></title>
	<style>
	*{
		opacity:0;
		transition: opacity 0.1s ease-in;
	}
</style>
<noscript>
	<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/libs.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri() ?>" />
</noscript>
<?php wp_head(); ?>

</head>
<body <?php body_class(); ?>>

	<div id="fb-root"></div>
	<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = 'https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.10';
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	<?php 
	$email = get_field('e-mail','options');
	$telefone = get_field('telefone','options');
	$telefone2 = get_field('telefone2','options');
	$whatsapp = get_field('whatsapp','options');
	$link_facebook = get_field('link_facebook','options');
	$link_instagram = get_field('link_instagram','options');
	?>
	<header class="header">
		<div class="top-header">
			<div class="container flex between">
				
				<ul class="contact-list">
					<li>
						<a href="mailto:<?php echo $email; ?>">
							<i class="fa fa-envelope" aria-hidden="true"></i>
							<?php echo $email; ?>
						</a>
					</li>
					<li>
						<a href="tel:+5511<?php echo $telefone; ?>">
							<i class="fa fa-phone" aria-hidden="true"></i>
							11 <?php echo $telefone; ?> / <?php echo $telefone2; ?>
						</a>
					</li>
					<li>
						<a href="tel:+5511<?php echo $whatsapp; ?>">
							<i class="fa fa-whatsapp" aria-hidden="true"></i>
							11 <?php echo $whatsapp; ?>
						</a>
					</li>
				</ul>
				<ul class="social-list">
					<li><a href="<?php echo $link_facebook; ?>"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
					<li><a href="<?php echo $link_instagram; ?>"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
				</ul>
			</div>
		</div>
		<div class="nav-header">
			<div class="container flex between">				
				<a href="<?php echo home_url(); ?>" class="logo">
					<img src="<?php echo get_template_directory_uri(); ?>/img/alianza-magistral.png" alt="Alianza Magistral" title="Alianza Magistral">
				</a>
				<input class="menu-btn" type="checkbox" id="menu-mobile" />
				<label class="menu-icon" for="menu-mobile"><span class="navicon"></span></label>
				<ul class="menu-list">
					<li id="menu-item-82" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-82"><a href="<?php echo home_url(); ?>/grupo-alianza/">Grupo Alianza</a></li>
					<li id="menu-item-81" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-81"><a href="<?php echo home_url(); ?>/insumos-cosmeticos/">Insumos Cosméticos</a>
						<div class="megamenu">
							<div class="container flex between">
								<ul class="mega-list">
									<?php 
									$query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
									if ( $query->have_posts() ) : 
										while ($query->have_posts()) : $query->the_post();
											$titulo = get_the_title();
											$link = get_the_permalink();
											?>
											<li class="mega-item"><a href="<?php echo $link; ?>"><?php echo $titulo; ?></a></li>
											<?php 
										endwhile;
									endif;
									wp_reset_postdata(); ?>            
								</ul>    	    				
							</div>
						</div>
						<!-- mega menu fim -->

					</li>
					<li id="menu-item-80" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-80"><a href="<?php echo home_url(); ?>/documentacoes/">Documentaçōes</a></li>
					<li id="menu-item-79" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-79"><a href="<?php echo home_url(); ?>/orcamento/">Orçamento</a></li>
					<li id="menu-item-78" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-78"><a href="<?php echo home_url(); ?>/contato/">Contato</a></li>
				</ul>

			</div>
		</div>
	</header>