<?php get_header(); 

/* Template Name: Destaques */

if (have_posts()) : 
  while (have_posts()) : the_post(); 

   $title = get_the_title();
   $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

 endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="product">
	<div class="container sidebar">
    <article class="content-full">
      <?php the_content(); ?>
    </article>
    <aside class="others-list">
      <h4>Confira todos os insumos da Alianza Magistral</h4>
      <ul>
        <?php 
        $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
        if ( $query->have_posts() ) : 
         while ($query->have_posts()) : $query->the_post();
           $titulo = get_the_title();
           $link = get_the_permalink();
           $idSide = get_the_ID();

           if($idPage == $idSide):
            ?>
            <li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
          <?php else: ?>
            <li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
            <?php 
          endif;
        endwhile;
      endif;
      wp_reset_postdata(); ?>            
    </ul>
  </aside>
</div>
</section>
<!-- modal padrão -->
<div id="modal" class="modal">
  <a href="#close-modal" title="Fechar" class="modal-close" data-izimodal-close="">x</a>
  <h2>Preencha os campos abaixo para efetuar o download</h2>
  <form action="#" method="POST" onsubmit="return EnviaFormDestaque()">
    <input class="input" type="text" name="name" id="name" placeholder="Nome">
    <input class="input" type="email" name="email" id="email" placeholder="Email">
    <input class="input" type="phone" name="phone" id="phone" placeholder="Telefone">
    <input class="input" type="phone" name="mobile" id="mobile" placeholder="Celular">
    <button class="btn solid" type="submit">
      Enviar
      <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
    </button>
  </form>
</div>
<?php get_footer(); ?>