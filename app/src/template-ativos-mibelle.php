<?php get_header(); 

/* Template Name: Ativos Mibelle */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

     $title = get_the_title();
     $id = get_the_ID();
     $idPage = $id;
     $ativos = get_field('insumos');

    endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="product">
	<div class="container sidebar">
        <article class="content-list">
			<ul>
				<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 21, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
	  	$titulo = get_the_title();
     $inci_name = get_field('inci_name');
      $link = get_the_permalink();
      $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

    ?>
                <li>
                	<picture>
				<img src="<?php echo $img[0]; ?>" alt="<?php echo $titulo; ?>">
            </picture>
					<h3><?php echo $titulo; ?></h3>
					<em><?php echo $inci_name; ?></em>
          <a href="<?php echo $link; ?>" class="btn solid">
						Saiba mais
						<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					</a>
				</li>
    <?php 
    endwhile;
    endif;
        wp_reset_postdata(); ?>            

			</ul>
        </article>
		<aside class="others-list">
			<h4>Confira todos os insumos da Alianza Magistral</h4>
			<ul>
<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
	  $titulo = get_the_title();
      $link = get_the_permalink();
      $idSide = get_the_ID();
    
    if($idPage == $idSide):
    ?>
				<li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
    <?php else: ?>
				<li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
    <?php 
    endif;
    endwhile;
    endif;
        wp_reset_postdata(); ?>            
			</ul>
		</aside>
	</div>
</section>


<?php get_footer(); ?>