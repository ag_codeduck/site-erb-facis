<?php get_header(); 

/* Template Name: Documentaçōes */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

     $title = get_the_title();
     $documentos_legislacao = get_field('documentos_legislacao');
     $documentos_licenciamento = get_field('documentos_licenciamento');
     $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

    endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="documentacoes">
	<div class="container sidebar">
    <article class="content-docs">
		            <picture>
				<img src="<?php echo $img[0]; ?>" alt="<?php echo $title; ?>">
            </picture>

    	<h2>Legislação</h2>
			<ul class="documentos">
				<?php foreach($documentos_legislacao as $documento_legislacao): ?>
					<li class="documento">
						<h3><?php echo $documento_legislacao['nome']; ?></h3>
						<a href="<?php echo $documento_legislacao['arquivo']; ?>" class="btn solid" target="_blank">
					Baixar
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
				</a>

					</li>
				<?php endforeach; ?>
			</ul>
    	<h2>Licenciamento</h2>
			<ul class="documentos">
				<?php foreach($documentos_licenciamento as $documento_licenciamento): ?>
					<li class="documento">
						<h3><?php echo $documento_licenciamento['nome']; ?></h3>
						<a href="<?php echo $documento_licenciamento['arquivo']; ?>" class="btn solid" target="_blank">
					Baixar
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
				</a>

					</li>
				<?php endforeach; ?>
			</ul>
			
        </article>
        <aside class="others-list">
			<h4>Confira todos os insumos da Alianza Magistral</h4>
			<ul>
<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
	  $titulo = get_the_title();
      $link = get_the_permalink();
      $idSide = get_the_ID();
    
    if($idPage == $idSide):
    ?>
				<li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
    <?php else: ?>
				<li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
    <?php 
    endif;
    endwhile;
    endif;
        wp_reset_postdata(); ?>            
			</ul>
		</aside>
	</div>
</section>
<?php get_footer(); ?>