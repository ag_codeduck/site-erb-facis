<?php get_header(); 

/* Template Name: Destaques */

if (have_posts()) : 
    while (have_posts()) : the_post(); 

       $title = get_the_title();
       $img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );
       $inci_name = get_field('inci_name');
       $concentracao = get_field('concentracao');
       $frase_destaque = get_field('frase_destaque');
       $brochura = get_field('brochura');
       $apresentacao = get_field('apresentacao');
       $lamina = get_field('lamina');
       global $post;
       $id = $post->post_parent;
       $idPage = $id;


   endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="product">
	<div class="container sidebar">
        <article class="content-full">
            <picture>
                <img src="<?php echo $img[0]; ?>" alt="<?php echo $title; ?>">
            </picture>
            <p><em><?php echo $inci_name; ?></em></p>
            <p><strong>Concentração de uso: </strong><?php echo $concentracao; ?></p>
            <h2><?php echo $frase_destaque; ?></h2>
            <?php the_content( ); ?>
            <div class="botoes">
            <?php if(!empty($lamina)){ ?>
                <a href="<?php echo $lamina; ?>" class="btn brochura" download >
                    Lamina
                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </a>
                <?php } if(!empty($brochura)){ ?>
                    <a href="<?php echo $brochura; ?>" class="btn brochura" download >
                    <!--<a href="javascript:void(0)" class="btn brochura trigger" data-pdf="<?php echo $brochura; ?>">-->
                        Brochura
                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </a>
                    <?php } if(!empty($apresentacao)){ ?>
                        <a href="javascript:void(0)" class="btn brochura trigger" data-pdf="<?php echo $apresentacao; ?>">
                           Apresentação
                           <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                       </a>
                   <?php } if(empty($brochura) && empty($lamina) && empty($apresentacao)) {?>
                    <a href="javascript:void(0)" class="btn solid">
                       Saiba mais
                       <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                   </a>
                   <?php } ?>
               </div>
           </article>
           <aside class="others-list">
             <h4>Confira todos os insumos da Alianza Magistral</h4>
             <ul>
                <?php 
                $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
                if ( $query->have_posts() ) : 
                   while ($query->have_posts()) : $query->the_post();
                     $titulo = get_the_title();
                     $link = get_the_permalink();
                     $idSide = get_the_ID();

                     if($idPage == $idSide):
                        ?>
                        <li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
                    <?php else: ?>
                        <li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
                        <?php 
                    endif;
                endwhile;
            endif;
            wp_reset_postdata(); ?>            
        </ul>
    </aside>
</div>
</section>
<!-- modal padrão -->
<div id="modal" class="modal">
    <a href="#close-modal" title="Fechar" class="modal-close" data-izimodal-close="">x</a>
    <h2>Preencha os campos abaixo para efetuar o download</h2>
    <form action="" method="POST" id="formModal">
        <input class="input" type="text" name="nome" id="name" placeholder="Nome" required>
        <input class="input" type="email" name="email" id="email" placeholder="Email" required>
        <input class="input" type="phone" name="telefone" id="phone" placeholder="Telefone" required>
        <input class="input" type="phone" name="celular" id="mobile" placeholder="Celular" required>
        <input type="hidden" name="ativo" value="Apresentação <?php echo $title; ?>">
        <button class="btn solid" type="submit">
            Enviar
            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
        </button>
    </form>
</div>
    <a href="" id="downloadPDF" download target="_blank"></a>
    <?php get_footer(); ?>