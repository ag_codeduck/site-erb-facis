<?php get_header(); ?>
<section class="heading">
  <div class="container">
    <hgroup>
      <h2><?php single_cat_title(); ?></h2>
    </hgroup>
  </div>
</section>
<section class="featured news">
  <div class="container flex">
    <ul class="news-list">
<?php 
$cont=0;
if (have_posts()) : while (have_posts()) : the_post();
        $cont++;
    $titulo = get_the_title();
      $link = get_the_permalink();
        $html = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

?>
      <li class="news-item">
        <figure>
          <img src="<?php echo $html[0]; ?>" alt="<?php echo $titulo; ?>">
        </figure>
        <div class="news-content">          
          <strong>
            <?php echo $titulo; ?>
          </strong>
          <?php the_excerpt(); ?>
          <div class="news-links">
            <a href="<?php echo $link; ?>" class="btn inverted">
              Saiba mais
              <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
            </a>
            
          </div>
        </div>
      </li>

            <?php endwhile; endif; ?>
          </ul>
    <div class="pull-right">
      <?php if(function_exists('wp_pagenavi')) { wp_pagenavi(); } ?>
    </div>
      </div>
</section>

  <?php get_footer(); ?>
  
