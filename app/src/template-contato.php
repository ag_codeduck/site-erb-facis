<?php get_header(); 

/* Template Name: Contato  */

if (have_posts()) : 
	while (have_posts()) : the_post(); 

		$title = get_the_title();
		$id = get_the_ID();
		$idPage = $id;
		$ativos = get_field('insumos');
		$img = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post-thumbnails' );

	endwhile; 
endif; 

?>
<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php echo $title; ?></h2>
		</hgroup>
	</div>
</section>
<section class="orcamento">
	<div class="container sidebar">
		<article class="content-full">
			<picture>
				<img src="<?php echo $img[0]; ?>" alt="<?php echo $title; ?>">
			</picture>

			<h2>Entre em contato conosco</h2>

			<div class="budget-form">			
				<form action="" method="POST" id="newCustomerForm">
					<input class="input" type="text" name="nome" id="name" placeholder="Nome" required>
					<input class="input" type="email" name="email" id="email" placeholder="Email" required>
					<input class="input" type="phone" name="telefone" id="phone" placeholder="Telefone" required>
					<input class="input" type="phone" name="celular" id="mobile" placeholder="Celular" required>
					<textarea class="textarea" name="mensagem" id="message" placeholder="Mensagem" required></textarea>
					<button class="btn solid" type="submit">
						Enviar
						<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
					</button>
				</form>
				<div class="alert success">
					<i class="fa fa-check" aria-hidden="true"></i>
					<p>Seu e-mail foi cadastrado</p>
					<p>com sucesso!</p>
				</div>

				<div class="alert error">
					<i class="fa fa-times" aria-hidden="true"></i>
					<p>Por favor, preencha</p>
					<p>todos os campos</p>
				</div>
			</div>
			<h2>Veja como chegar</h2>
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14617.229829080443!2d-46.663913240809286!3d-23.664960463666542!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce5ab13fb3879d%3A0x83c2796e75e59b65!2sRua+Margarida+Zingg%2C+245+-+Jabaquara%2C+S%C3%A3o+Paulo+-+SP%2C+04385-080!5e0!3m2!1spt-BR!2sbr!4v1512578885188" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</article>
		<aside class="others-list">
			<h4>Confira todos os insumos da Alianza Magistral</h4>
			<ul>
				<?php 
				$query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
				if ( $query->have_posts() ) : 
					while ($query->have_posts()) : $query->the_post();
						$titulo = get_the_title();
						$link = get_the_permalink();
						$idSide = get_the_ID();

						if($idPage == $idSide):
							?>
							<li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
						<?php else: ?>
							<li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
							<?php 
						endif;
					endwhile;
				endif;
				wp_reset_postdata(); ?>            
			</ul>
		</aside>
	</div>
</section>


<?php get_footer(); ?>