<?php get_header(); ?>
<?php if (have_posts()) : ?>

<!-- heading -->
<section class="heading">
	<div class="container">
		<hgroup>
			<h2><?php $mySearch =& new WP_Query("s=$s & showposts=-1 & order=ASC & orderby=title"); $num = $mySearch->post_count; echo $num.' resultados para sua busca por: '; the_search_query();?></h2>
		</hgroup>
	</div>
</section>
<section class="documentacoes">
	<div class="container sidebar">
    <article class="content-docs">
			<ul class="documentos">
				<?php $cont = 0; while (have_posts()) : the_post(); $cont++; 

?>
         <li class="documento">
						<h3><?php the_title(); ?></h3>
						<a href="<?php the_permalink(); ?>" class="btn solid" target="_blank">
					Visualizar
					<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
				</a>

					</li>
<?php endwhile;?>
			</ul>
			
        </article>
<?php else : ?>
<section class="heading">
	<div class="container">
		<hgroup>
			<h2>Sua pesquisa por <?php the_search_query();?> não encontrou nenhum resultado.</h2>
		</hgroup>
	</div>
</section>
<section class="documentacoes">
	<div class="container sidebar">
    <article class="content-list">
			<h3>Sugestões:</h3>
<p>  Certifique-se de que todas as palavras estão escritas corretamente..</p>
   <p>  Tente palavras-chave diferentes..</p>
   <p>  Tente palavras-chave mais gerais.</p>
</ul>
			
        </article>
<?php endif; ?>

        <aside class="others-list">
			<h4>Confira todos os insumos da Alianza Magistral</h4>
			<ul>
<?php 
   $query = new WP_Query( array( 'post_type' => 'page', 'post_parent' => 11, 'orderby'=> 'title', 'order' => 'ASC', 'posts_per_page'    => -1));
    if ( $query->have_posts() ) : 
	while ($query->have_posts()) : $query->the_post();
	  $titulo = get_the_title();
      $link = get_the_permalink();
      $idSide = get_the_ID();
    
    if($idPage == $idSide):
    ?>
				<li><strong><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></strong></li>
    <?php else: ?>
				<li><a href="<?php echo $link; ?>"><i class="fa fa-caret-right" aria-hidden="true"></i><?php echo $titulo; ?></a></li>
    <?php 
    endif;
    endwhile;
    endif;
        wp_reset_postdata(); ?>            
			</ul>
		</aside>
	</div>
</section>
<?php get_footer(); ?>